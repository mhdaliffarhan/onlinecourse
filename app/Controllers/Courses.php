<?php

namespace App\Controllers;

use App\Models\CourseModel;

class Courses extends BaseController
{

  protected $courseModel;

  public function __construct()
  {
    $this->courseModel = new CourseModel;
  }

  public function index()
  {
    $course = $this->courseModel->findAll();
    $data = [
      'title' => 'Tabel Course',
      'course' => $course
    ];

    // dd($course);

    return view('admin/CoursesTable', $data);
  }

  public function save()
  {
    $this->courseModel->save([
      'course_name' => $this->request->getVar('course_name'),
      'course_type' => $this->request->getVar('course_type'),
      'course_price' => $this->request->getVar('course_price'),
      'course_desc' => $this->request->getVar('course_desc')
    ]);
    return redirect()->to('http://localhost:8080/Courses');
  }
}
