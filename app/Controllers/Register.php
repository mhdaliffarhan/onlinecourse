<?php

namespace App\Controllers;

class Register extends BaseController
{
  function __construct()
  {
    $this->userModel = new \App\Models\UserModel();
  }

  public function index()
  {
    $data = [
      'title' => 'Register',
    ];
    return view('register/index', $data);
  }

  public function save()
  {
    $this->userModel->save([
      'namalengkap' => $this->request->getVar('namalengkap'),
      'username' => $this->request->getVar('username'),
      'email' => $this->request->getVar('email'),
      'password' => $this->request->getVar('password'),
      'cpassword' => $this->request->getVar('cpassword')
    ]);
    // dd($this->request->getVar());
    return redirect()->to('http://localhost:8080/login');
  }

  // public function ngeregister()
  // {
  //     $url = "http://localhost/kuliah/UTS-PPK/backend/public/register";
  //     $data = [
  //         'namadepan' => $this->request->getVar('namadepan'),
  //         'namabelakang' => $this->request->getVar('namabelakang'),
  //         'nim' => $this->request->getVar('nim'),
  //         'email' => $this->request->getVar('email'),
  //         'password' => $this->request->getVar('password'),
  //         'cpassword' => $this->request->getVar('cpassword'),
  //         'member' => "member"
  //     ];
  //     $client = \Config\Services::curlrequest();
  //     $response = $client->request('POST', $url, [
  //         'form_params' => $data,
  //         'http_errors' => false
  //     ]);
  //     $result = json_decode($response->getBody(), true);
  //     $messages = $result['messages'];
  //     if ($result['status'] == 200) {
  //         session()->setFlashdata('register', 'Berhasil membuat akun');
  //         return redirect()->to(base_url('login'));
  //     } else {
  //         session()->setFlashdata('gagal_register', $messages);
  //         return redirect()->to(base_url('register'));
  //     }



  // if ($messages['nim'] == "The nim field must contain a unique value.") {
  //     session()->setFlashdata('gagal_nim', 'NIM sudah terdaftar');
  //     return redirect()->to(base_url('register'));
  // } else 
  // if ($messages['email'] == "The email field must contain a unique value.") {
  //     session()->setFlashdata('gagal_email', 'Email sudah terdaftar');
  //     return redirect()->to(base_url('register'));
  // }
  // if ($messages[])
  // else if ($messages['password'] == "The password field must be at least 4 characters in length.") {
  //     session()->setFlashdata('gagal_pass', 'Password minimal 4 karakter');
  //     return redirect()->to(base_url('register'));
  // }
  // else if ($messages['cpassword'] == "The cpassword field does not match the password field.") {
  //     session()->setFlashdata('gagal_cpass', 'Konfirmasi password tidak sesuai');
  //     return redirect()->to(base_url('register'));
  // }
}
