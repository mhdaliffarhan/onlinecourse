<?php

namespace App\Controllers;

class Users extends BaseController
{
  function __construct()
  {
    $this->model = new \App\Models\userModel();
  }

  public function index()
  {
    $data = [
      'title' => 'Tabel User',
    ];
    return view('admin/usersTable', $data);
  }
}
