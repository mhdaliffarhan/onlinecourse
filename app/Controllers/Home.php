<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Beranda',
        ];
        return view('beranda/index', $data);
    }

    public function tentang()
    {
        $data = [
            'title' => 'Tentang',
        ];
        return view('tentang/index', $data);
    }
}
