<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>


<div class="row justify-content-center pt-2">
  <div class="col-11 col-lg-10 col-sm-10">
    <!-- DATA TABLE -->
    <section class="section">
      <div class="card">
        <div class="card-header">
          <!-- SEARCH BAR -->
          <div class="row justify-content-center pt-2">
            <div class="col-12 col-lg-11">
              <form action="" method="get">
                <div class="row mb-2 d-flex row justify-content-center">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" name="katakunci" placeholder="Nama kursus" aria-label="Recipient's username" aria-describedby="button-addon2">
                    <button class="btn btn-primary" type="button" id="button-addon2"> Cari kursus </button>
                  </div>
              </form>
            </div>
          </div>
          <!-- END OF SEARCH BAR  -->
          <!-- MODAL TAMBAH DATA-->
          <div class="row justify-content-end">
            <div class="col-12 col-lg-2 pt-2">
              <button type="button" class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#Formtambahdata">
                + Tambah Kursus
              </button>
              <!-- Tambah Data Modal -->
              <div class="modal fade text-left" id="Formtambahdata" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel33">Tambah Kursus</h4>
                      <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                      </button>
                    </div>
                    <form action="/courses/save" method="post">
                      <?= csrf_field() ?>
                      <div class="modal-body text-start">
                        <label>Nama kursus</label>
                        <div class="form-group">
                          <input type="text" placeholder="nama kursus" class="form-control" name="course_name">
                        </div>
                        <label>Tipe</label>
                        <div class="form-group">
                          <input type="text" placeholder="tipe" class="form-control" name="course_type">
                        </div>
                        <label>Harga</label>
                        <div class="form-group">
                          <input type="text" placeholder="harga" class="form-control" name="course_price">
                        </div>
                        <label>Deskripsi</label>
                        <div class="form-group">
                          <input type="text" placeholder="deskripsi" class="form-control" name="course_desc">
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-primary ml-1">
                          <i class="bx bx-check d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">+ Tambahkan</span>
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- END OF MODAL -->
        </div>
        <div class="card-body">
          <table class="table table-striped align-contet" id="table1">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kursus</th>
                <th>Tipe</th>
                <th>Harga</th>
                <th>Deskripsi</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($course as $c) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $c['course_name']; ?></td>
                  <td><?= $c['course_type']; ?></td>
                  <td><?= $c['course_price']; ?></td>
                  <td><?= $c['course_desc']; ?></td>
                  <td class="text-center">
                    <!-- MODAL EDIT DATA-->
                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#inlineForm">
                      EDIT
                    </button>
                    <!-- Tambah Data Modal -->
                    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel33">Login Form </h4>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                              <i data-feather="x"></i>
                            </button>
                          </div>
                          <form action="#">
                            <div class="modal-body text-start">
                              <label>Email: </label>
                              <div class="form-group">
                                <input type="text" placeholder="Email Address" class="form-control">
                              </div>
                              <label>Password: </label>
                              <div class="form-group">
                                <input type="password" placeholder="Password" class="form-control">
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Close</span>
                              </button>
                              <button type="button" class="btn btn-primary ml-1" data-bs-dismiss="modal">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">login</span>
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <!-- END OF MODAL -->
                    <!-- MODAL HAPUS DATA-->
                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#inlineForm">
                      HAPUS
                    </button>
                    <!-- Tambah Data Modal -->
                    <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel33">Login Form </h4>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                              <i data-feather="x"></i>
                            </button>
                          </div>
                          <form action="#">
                            <div class="modal-body">
                              <label>Email: </label>
                              <div class="form-group">
                                <input type="text" placeholder="Email Address" class="form-control">
                              </div>
                              <label>Password: </label>
                              <div class="form-group">
                                <input type="password" placeholder="Password" class="form-control">
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                                <i class="bx bx-x d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Close</span>
                              </button>
                              <button type="button" class="btn btn-primary ml-1" data-bs-dismiss="modal">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">login</span>
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <!-- END OF MODAL -->
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- END OF DATA TABLE -->
  </div>
</div>
<script>
  $('.ui.dropdown')
    .dropdown({
      clearable: true,
      fullTextSearch: true,
    });
</script>


<?php $this->endSection(); ?>