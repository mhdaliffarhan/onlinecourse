<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>


<div class="row justify-content-center pt-2">
  <div class="col-11 col-lg-10 col-sm-10">
    <!-- DATA TABLE -->
    <section class="section">
      <div class="card">
        <div class="card-header">
          <!-- SEARCH BAR -->
          <form action="" method="get">
            <div class="row mb-2 d-flex row justify-content-center">
              <div class="input-group mb-3">
                <input type="text" class="form-control" name="katakunci" placeholder="Nama/Email User..." aria-label="Recipient's username" aria-describedby="button-addon2">
                <button class="btn btn-primary" type="button" id="button-addon2"> Cari User </button>
              </div>
          </form>
          <!-- END OF SEARCH BAR  -->
        </div>
        <div class="card-body">
          <table class="table table-striped align-contet" id="table1">
            <thead>
              <tr>
                <th>Nama Lengkap</th>
                <th>Username</th>
                <th>Email</th>
                <th class="text-center">Role</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>M Alif Farhan</td>
                <td>mhdaliffarhan</td>
                <td>mhdaliffarhan22@gmail.com</td>
                <td class="text-center">
                  <span class="badge bg-info">User</span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

    </section>
    <!-- END OF DATA TABLE -->
  </div>
</div>

<script>
  $('.ui.dropdown')
    .dropdown({
      clearable: true,
      fullTextSearch: true,
    });
</script>


<?php $this->endSection(); ?>