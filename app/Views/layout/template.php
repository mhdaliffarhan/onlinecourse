<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= $title; ?> | STIS ACADEMY</title>

  <!-- CDN Bootstrap Icon -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">

  <!-- CSS Style Tampilan -->
  <link rel="stylesheet" href="assets/css/main/app.css">
  <link rel="stylesheet" href="assets/css/main/app-dark.css">

  <!-- Assets Logo  -->
  <link rel="shortcut icon" href="assets/images/logo/favicon.svg" type="image/x-icon">
  <link rel="shortcut icon" href="assets/images/logo/favicon.png" type="image/png">
  <link rel="stylesheet" href="assets/css/shared/iconly.css">

</head>

<body>
  <?= $this->include('layout/navbar'); ?>

  <!-- CONTENT START HERE -->
  <div class="page-heading">
    <div style="margin-bottom: 3rem;"></div>
    <div class="page-title">
      <div class="row justify-content-center">
        <div class="col-10 col-lg-10">
          <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
              <h3><?= $title ?></h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
              <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <?= $title ?></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <?php $this->renderSection('content'); ?>
      <!-- END OF CONTENT -->


    </div>



    <?= $this->include('layout/footer'); ?>

    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/app.js"></script>

</body>

</html>