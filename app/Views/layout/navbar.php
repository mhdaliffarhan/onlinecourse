<div id="app">
  <div id="main" class="layout-horizontal">
    <header class="mb-5">
      <div class="header-top">
        <div class="container">
          <!-- LOGO -->
          <div class="logo">
            <a href="/"><img src="assets/images/img/stis.webp" alt="Logo"> STIS ACADEMY</a>
          </div>
          <!-- PROFIL  -->
          <div class="header-top-right">
            <div class="dropdown">
              <a href="#" id="topbarUserDropdown" class="user-dropdown d-flex align-items-center dropend dropdown-toggle " data-bs-toggle="dropdown" aria-expanded="false">
                <div class="avatar avatar-md2">
                  <img src="assets/images/faces/1.jpg" alt="Avatar">
                </div>
                <div class="text">
                  <h6 class="user-dropdown-name">M Alif Farhan</h6>
                  <p class="user-dropdown-status text-sm text-muted">Member</p>
                </div>
              </a>
              <ul class="dropdown-menu dropdown-menu-end shadow-lg" aria-labelledby="topbarUserDropdown">
                <li><a class="dropdown-item" href="#">My Account</a></li>
                <li><a class="dropdown-item" href="#">Settings</a></li>
                <li>
                  <hr class="dropdown-divider">
                </li>
                <li><a class="dropdown-item" href="auth-login.html">Logout</a></li>
              </ul>
            </div>

            <!-- Burger button responsive -->
            <a href="#" class="burger-btn d-block d-xl-none">
              <i class="bi bi-justify fs-3"></i>
            </a>
          </div>
        </div>
      </div>
      <nav class="main-navbar">
        <div class="container">
          <ul>
            <li class="menu-item  ">
              <a href="http://localhost:8080/Home" class='menu-link'>
                <span>Beranda</span>
              </a>
            </li>

            <li class="menu-item  ">
              <a href="http://localhost:8080/tentang" class='menu-link'>
                <span>Tentang</span>
              </a>
            </li>

            <li class="menu-item  has-sub">
              <a href="#" class='menu-link'>
                <span>Admin</span>
              </a>
              <div class="submenu ">
                <!-- Wrap to submenu-group-wrapper if you want 3-level submenu. Otherwise remove it. -->
                <div class="submenu-group-wrapper">
                  <ul class="submenu-group">
                    <li class="submenu-item  ">
                      <a href="http://localhost:8080/Users" class='submenu-link'>Tabel Users</a>
                    </li>
                    <li class="submenu-item  ">
                      <a href="http://localhost:8080/Courses" class='submenu-link'>Tabel Courses</a>
                    </li>
                  </ul>
                </div>
              </div>
            </li>

          </ul>
        </div>
      </nav>
    </header>
  </div>
</div>



<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/app.js"></script>
<script src="assets/js/pages/horizontal-layout.js"></script>

<script src="assets/extensions/apexcharts/apexcharts.min.js"></script>
<script src="assets/js/pages/dashboard.js"></script>