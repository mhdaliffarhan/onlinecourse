<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>


<div class="row mb-4 d-flex justify-content-center">
  <div class="col-12 col-md-10">
    <div class="row">
      <div class="col-12 col-lg-9 col-sm-12 mb-2">
        <div class="card">
          <div class="card-content">
            <div class="card-body">
              <p class="card-text">
                Belajar budidaya kode (coding) dengan tutorial yang mudah dipahami di STIS Academy!
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-3 col-sm-12 mb-2">
        <div class="card">
          <div class="card-body px-4 py-4-5">
            <div class="row">
              <div class="col-md-3 col-lg-12 col-xl-12 col-xxl-3 d-flex justify-content-start">
                <div class="stats-icon purple mb-2">
                  <!-- <i class="iconly-boldProfile"></i> -->
                  <span><b><i class="bi bi-person-fill"></i></b></span>
                </div>
              </div>
              <div class="col-md-9 col-lg-12 col-xl-12 col-xxl-9">
                <h6 class="text-muted font-semibold">Banyak Pengguna</h6>
                <h6 class="font-extrabold mb-0">12</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="section">
  <div class="row justify-content-center">
    <div class="col-12 col-md-8">
      <div class="pricing">
        <div class="row align-items-center justify-content-center">
          <div class="col-10 col-md-4 px-0 mb-4">
            <div class="card">
              <div class="card-header text-center">
                <h4 class="card-title">Front-End Web Developer</h4>
                <p class="text-center pt-2">Kurikulum disusun oleh Dicoding dan pelaku industri di bidang Web Development. Siswa dipersiapkan untuk menjadi Front-End Web Developer sesuai standar kebutuhan industri.</p>
              </div>
              <h1 class="price">1.7 jt</h1>
              <ul>
                <li><i class="bi bi-check-circle"></i>Dasar Pemograman Web</li>
                <li><i class="bi bi-check-circle"></i>Membuat Front-End Web untuk Pemula</li>
                <li><i class="bi bi-check-circle"></i>Fundamental Front-End Web Development</li>
                <li><i class="bi bi-check-circle"></i>Menjadi Front-End Web Developer Expert</li>
              </ul>
              <div class="card-footer">
                <button class="btn btn-primary btn-block">Order Now</button>
              </div>
            </div>
          </div>
          <!-- Highlighted Pricing -->
          <div class="col-10 col-md-4 px-0  position-relative z-1 mb-4">
            <div class="card card-highlighted shadow-lg">
              <div class="card-header text-center">
                <h4 class="card-title">Android Developer</h4>
                <p class="text-center pt-2">Kurikulum didesain dengan persetujuan dari Tim Google Android untuk mempersiapkan developer Android standar Global. Dicoding adalah Google Developer Authorized Training Partner.</p>
              </div>
              <h1 class="price text-white">2.5 jt</h1>
              <ul>
                <li><i class="bi bi-check-circle"></i>Dasar Pemograman Kotlin</li>
                <li><i class="bi bi-check-circle"></i>Membuat Aplikasi Android Pemula</li>
                <li><i class="bi bi-check-circle"></i>Fundamental Aplikasi Android</li>
                <li><i class="bi bi-check-circle"></i>Pengembangan Aplikasi Android Intermediate</li>
                <li><i class="bi bi-check-circle"></i>Prinsip Pemograman SOLID</li>
                <li><i class="bi bi-check-circle"></i>Android Developer ExpertS</li>
              </ul>
              <div class="card-footer">
                <button class="btn btn-outline-white btn-block">Order Now</button>
              </div>
            </div>
          </div>
          <div class="col-10 col-md-4 px-0 mb-5">
            <div class="card">
              <div class="card-header text-center">
                <h4 class="card-title">Back-End Developer</h4>
                <p class="text-center">Kurikulum disusun oleh Dicoding bersama AWS beserta pelaku industri Back-End Development. Siswa dipersiapkan untuk menjadi Back-End Developer sesuai kebutuhan industri.</p>
              </div>
              <h1 class="price">2.1 jt</h1>
              <ul>
                <li><i class="bi bi-check-circle"></i>Cloud Practitioner Essentials</li>
                <li><i class="bi bi-check-circle"></i>Dasar Pemograman JavaScript</li>
                <li><i class="bi bi-check-circle"></i>Membuat Aplikasi Back-End untuk Pemula</li>
                <li><i class="bi bi-check-circle"></i>Membangun Arsitektur Cloud di AWS</li>
                <li><i class="bi bi-check-circle"></i>Fundamental Aplikasi Back-End</li>
                <li><i class="bi bi-check-circle"></i>Back-End Developer Expert</li>
              </ul>
              <div class="card-footer">
                <button class="btn btn-primary btn-block">Order Now</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php $this->endSection(); ?>