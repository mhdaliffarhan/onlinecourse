<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= $title; ?> | STIS ACADEMY</title>

  <!-- CDN Bootstrap Icon -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">

  <!-- CSS Style Tampilan -->
  <link rel="stylesheet" href="assets/css/main/app.css">
  <link rel="stylesheet" href="assets/css/main/app-dark.css">

  <link rel="shortcut icon" href="assets/images/logo/favicon.svg" type="image/x-icon">
  <link rel="shortcut icon" href="assets/images/logo/favicon.png" type="image/png">

</head>

<body>
  <div class="row d-flex justify-content-center pt-4">
    <div class="col-10 col-md-4 p-4">
      <div id="auth">
        <div class="card">
          <div class="card-content">
            <div class="card-body m-4">
              <h1 class="auth-title text-center mb-2">LOGIN</h1>
              <p class="auth-subtitle text-center mb-4">Log in with your data that you entered during registration.</p>

              <form action="http://localhost:8080/home">
                <div class="form-group position-relative has-icon-left">
                  <input type="text" class="form-control form-control-xl" placeholder="Username">
                  <div class="form-control-icon">
                    <i class="bi bi-person"></i>
                  </div>
                </div>
                <div class="form-group position-relative has-icon-left">
                  <input type="password" class="form-control form-control-xl" placeholder="Password">
                  <div class="form-control-icon">
                    <i class="bi bi-shield-lock"></i>
                  </div>
                </div>
                <div class="form-check form-check-lg d-flex align-items-end">
                  <input class="form-check-input me-2" type="checkbox" value="" id="flexCheckDefault">
                  <label class="form-check-label text-gray-600" for="flexCheckDefault">
                    Keep me logged in
                  </label>
                </div>
                <button class="btn btn-primary btn-block btn-lg shadow-lg mt-4">Log in</button>
              </form>
              <div class="text-center mt-2 text-lg fs-4">
                <p class="text-gray-600">Don't have an account? <a href="/register" class="font-bold">Register</a>.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="assets/js/bootstrap.js"></script>
  <script src="assets/js/app.js"></script>
</body>