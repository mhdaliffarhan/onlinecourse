<?= $this->extend('/layout/template'); ?>
<?= $this->section('content'); ?>

<div class="row d-flex justify-content-center">
  <div class="col-10 col-lg-4 col-md-6">
    <div class="card">
      <div class="card-content">
        <img src="assets/images/img/me.jpg" class="card-img-top img-fluid" alt="singleminded">
        <div class="card-body">
          <h5 class="card-title">M Alif Farhan</h5>
        </div>

      </div>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">222011401</li>
        <li class="list-group-item">3SI3</li>
        <li class="list-group-item">Politeknik Statistika STIS</li>
      </ul>
    </div>
  </div>
  <div class="col-10 col-lg-4 col-md-6">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <h5 class="card-title">Tentang</h5>
          <p class="card-text">
            "Kita memiliki kebiasaan membesar-besarkan kesedihan. Kita tercabik di antara hal-hal masa kini dan hal-hal yang baru akan terjadi. Pikirkan apakah sudah ada bukti pasti mengenai kesusahan masa depan. Karena sering kali kita lebih disusahkan kekhawatiran kita sendiri."
            - Seneca
          </p>
          <p class="card-text">

          </p>
        </div>
      </div>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">Pekanbaru, 22 Mei 2002</li>
        <li class="list-group-item">+62 895602588736</li>
        <li class="list-group-item">Padang, Sumatera Barat</li>
      </ul>
    </div>
  </div>
</div>
<?php $this->endSection(); ?>