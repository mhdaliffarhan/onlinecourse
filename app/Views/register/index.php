<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= $title; ?> | STIS ACADEMY</title>
  <!-- CDN Bootstrap Icon -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">

  <!-- CSS Style Tampilan -->
  <link rel="stylesheet" href="assets/css/main/app.css">
  <link rel="stylesheet" href="assets/css/main/app-dark.css">

  <link rel="shortcut icon" href="assets/images/logo/favicon.svg" type="image/x-icon">
  <link rel="shortcut icon" href="assets/images/logo/favicon.png" type="image/png">

</head>

<body>
  <div class="row d-flex justify-content-center">
    <div class="col-10 col-md-4 p-4">
      <div id="auth">
        <div class="card">
          <div class="card-content">
            <div class="card-body m-4">
              <h1 class="auth-title text-center mb-2">REGISTER</h1>
              <p class="auth-subtitle text-center mb-4">Input your data to register to our website.</p>
              <form action="/register/save" method="post">
                <?= csrf_field() ?>
                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control form-control-xl" placeholder="Nama Lengkap" name="namalengkap" autofocus>
                  <div class="form-control-icon">
                    <i class="bi bi-person"></i>
                  </div>
                </div>
                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control form-control-xl" placeholder="email" name="email" autofocus>
                  <div class="form-control-icon">
                    <i class="bi bi-person"></i>
                  </div>
                </div>
                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control form-control-xl" placeholder="Username" name="username">
                  <div class="form-control-icon">
                    <i class="bi bi-person"></i>
                  </div>
                </div>
                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="password" class="form-control form-control-xl" placeholder="Password" name="password">
                  <div class="form-control-icon">
                    <i class="bi bi-shield-lock"></i>
                  </div>
                </div>
                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="password" class="form-control form-control-xl" placeholder="Confirm Password" name="cpassword">
                  <div class="form-control-icon">
                    <i class="bi bi-shield-lock"></i>
                  </div>
                </div>
                <button class="btn btn-primary btn-block btn-lg shadow-lg">Register</button>
              </form>
              <div class="text-center mt-2 text-lg fs-4">
                <p class='text-gray-600'>Already have an account? <a href="/login" class="font-bold">Login</a>.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="assets/js/bootstrap.js"></script>
  <script src="assets/js/app.js"></script>
</body>

</html>