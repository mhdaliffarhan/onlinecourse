<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
  protected $table = 'tb_users';
  protected $primarykey = 'user_id';
  protected $allowedFields = ['namalengkap', 'username', 'email', 'password', 'cpassword'];
  // protected $useTimestamps = true;

  public function getUser($id = false)
  {
    if ($id == false) {
      return $this->findAll();
    } else {
      return $this->where(['id' => $id])->first();
    }
  }
}
