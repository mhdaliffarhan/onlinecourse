<?php

namespace App\Models;

use CodeIgniter\Model;

class CourseModel extends Model
{
  protected $table = 'tb_courses';
  protected $primarykey = 'course_id';
  protected $allowedFields = ['course_name', 'course_type', 'course_price', 'course_desc'];

  public function getCourse($id = false)
  {
    if ($id == false) {
      return $this->findAll();
    } else {
      return $this->where(['id' => $id])->first();
    }
  }
}
